import React, { Component } from 'react'

class SearchForm extends Component {
  render() {
    return (
      <form role="search">
        <div className="search-control">
          <input
            type="search"
            id="site-search"
            placeholder="Search the site..."
            aria-label="Search through site content"
          />
          <button>Search</button>
        </div>
      </form>
    )
  }
}

export default SearchForm
