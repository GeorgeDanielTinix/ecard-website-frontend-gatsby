export const hallMarkTheme = {
  white: '#f8f9fa',
  hallmarkMagenta: '##cf1b74',
  hallmarkPurple: '#613790',
  darkGrey: '##252525',
  midGrey: '#414141',
}
