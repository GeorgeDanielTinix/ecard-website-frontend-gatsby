import axios from 'axios'

function post(url, data) {
  const fullUrl = `${process.env.REACT_APP_STATIC_BASE_URL}${url}`
  return axios.post(url, data).catch(function(error) {
    console.log(error)
  })
}

function get(url, data) {
  const fullUrl = `${process.env.REACT_APP_STATIC_BASE_URL}${url}`
  return axios
    .get(fullUrl, data)
    .then(function(response) {
      return response
    })
    .catch(function(error) {
      console.log(error)
    })
}

function getLogoName(url, data) {
  const fullUrl = `${process.env.REACT_APP_API_BASE_URL}${url}`
  return axios
    .get(fullUrl, data)
    .then(function(response) {
      return response.data.desktopImage
    })
    .catch(function(error) {
      console.log(error)
    })
}

export const getSiteLogoUrl = () =>
  getLogoName(`site_logo_by_date`).then(response => {
    return `${
      process.env.REACT_APP_STATIC_BASE_URL
    }web/images/site-logos/${response}`
  })

export const loginUser = user => post(`users/sign_in`, { user })
