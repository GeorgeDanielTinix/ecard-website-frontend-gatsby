import React, { Component } from 'react'
import { getSiteLogoUrl } from '../api'
import styled from 'react-emotion'

const Logocell = styled('div')`
  padding: 14px 2% 5px;
  width: 13%;
`

class SiteLogo extends Component {
  state = {
    logoUrl: '/images/ecards-logo-black-bigger-hallmark.png',
  }

  componentWillMount() {
    getSiteLogoUrl().then(logoUrl => {
      console.log('LOGO URL', logoUrl)
      this.setState({ logoUrl })
    })
  }

  render() {
    // FOR CHECK LATER PER OLD SITE
    // const { desktop } = this.props
    return (
      <React.Fragment>
        <Logocell className="logo-cell">
          <a href="#">
            <img
              className="ecards-logo"
              src={this.state.logoUrl}
              alt="Hallmark eCards Logo"
            />
          </a>
        </Logocell>
      </React.Fragment>
    )
  }
}

export default SiteLogo
