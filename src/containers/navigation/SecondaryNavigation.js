import React from 'react'
import SearchForm from '../../utils/SearchForm'
import styled from 'react-emotion'
import { css } from 'emotion'

const LoginSearchWrapper = styled('div')`
  position: absolute;
  right: 8px;
  top: 12px;
`

const LoginSection = styled('div')`
  position: relative;
  display: inline-block;
`

const SearchSection = styled('div')`
  display: inline-block;
  position: relative;
  margin-left: 5px;
`

const userButton = css`
  background: #9bdae1;
  font-family: 'gothambook';
  color: #06adbf;
  padding: 8px 15px;
  font-size: 0.9em;
`

const SecondaryNavigation = props => {
  // const { level, message } = props
  // const classNames = ['alert', 'alert-' + level]
  return (
    <LoginSearchWrapper className="login-search-section">
      <LoginSection>
        <a className={userButton}>LOGIN</a>
        <a className={userButton}>JOIN</a>
      </LoginSection>
      <SearchSection>
        <SearchForm />
      </SearchSection>
    </LoginSearchWrapper>
  )
}

export default SecondaryNavigation

// login-search-section
