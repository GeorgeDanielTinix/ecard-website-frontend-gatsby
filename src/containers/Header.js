import React, { Component } from 'react'
import { UserContext } from '../context/UserContext'
//import { getSiteLogoUrl } from '../api'
import SiteLogo from '../components/SiteLogo'
import PrimaryNavigation from './navigation/PrimaryNavigation'
import SecondaryNavigation from './navigation/SecondaryNavigation'

import styled from 'react-emotion'

const Container = styled('div')`
  background: ${props => props.theme.white};
`

class Header extends Component {
  state = {
    logoUrl: null,
  }

  componentDidMount() {
    // getSiteLogoUrl().then(logoUrl => {
    //   this.setState({ logoUrl })
    // })
  }

  render() {
    return (
      <Container>
        <UserContext.Consumer>
          {({ user }) => <div>{user.email}</div>}
        </UserContext.Consumer>
        <div
          style={{
            margin: '0 auto',
            maxWidth: 960,
            padding: '1.45rem 1.0875rem',
          }}
        >
          <SiteLogo />
          <SecondaryNavigation />
          <PrimaryNavigation />
        </div>
      </Container>
    )
  }
}

export default Header
